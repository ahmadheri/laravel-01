<?php

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/senddata', 'AuthController@sendData')->name('sendData');
Route::get('/welcome', 'HomeController@welcome')->name('welcome');


