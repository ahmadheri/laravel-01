<html>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="{{ route('sendData') }}" method="POST">
        @csrf

        <p>First name:</p>
        <input type="text" name="firstname" id="firstName"> <br>
    
        <p>Last name:</p>
        <input type="text" name="lastname" id="lastName">
        
        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label> <br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label> <br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label> <br>

        <p>Nationality</p>
        <select name="nationalities" id="nationalities">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapore">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="bahasaIndonesia" name="bahasaIndonesia" value="bahasaIndonesia">
        <label for="bahasaIndonesia">Bahasa Indonesia</label> <br>
        <input type="checkbox" id="english" name="english" value="english">
        <label for="english">English</label> <br>
        <input type="checkbox" id="Other" name="other" value="other">
        <label for="Other">Other</label> <br>
        
        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br><br>

        <button type="submit">Sign Up</button>

    </form>
    
</html>